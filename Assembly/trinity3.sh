#!/bin/bash
#SBATCH --job-name=Trinity3
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=200G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "trinity3.sh"

module load trinity/2.6.6

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC2A_index4_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC2A_index4_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_PC2A_index4 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC2J_index35_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC2J_index35_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_PC2J_index35 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC3A_index6_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC3A_index6_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_PC3A_index6 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_PC3J_index7_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_PC3J_index7_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 200G \
        --output trinity_PC3J_index7 \
        --full_cleanup

