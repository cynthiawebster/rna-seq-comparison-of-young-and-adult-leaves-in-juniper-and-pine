#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "Trinity"


module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1


rnaQUAST.py --transcripts ../Assembly/trinity_JF1A_index6.Trinity.fasta ../Assembly/trinity_JF2A_index7.Trinity.fasta ../Assembly/trinity_JF3A_index12.Trinity.fasta ../Assembly/trinity_PC1A_index2.Trinity.fasta ../Assembly/trinity_PC2A_index4.Trinity.fasta ../Assembly/trinity_PC3A_index6.Trinity.fasta ../Assembly/trinity_JF1J_index4.Trinity.fasta ../Assembly/trinity_JF2J_index5.Trinity.fasta ../Assembly/trinity_JF3J_index2.Trinity.fasta ../Assembly/trinity_PC1J_index12.Trinity.fasta ../Assembly/trinity_PC2J_index35.Trinity.fasta ../Assembly/trinity_PC3J_index7.Trinity.fasta  \
        --gene_mark \
        --threads 8 \
        --output_dir results
