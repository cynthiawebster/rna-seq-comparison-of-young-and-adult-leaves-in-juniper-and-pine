#!/bin/bash
#BATCH --job-name=vsearch_JJ
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch_JJ"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../Coding_Region/trinity_combine_JJ.fasta.transdecoder.cds \
        --id 0.90 \
        --centroids centroids_JJ.fasta \
        --uc clusters_JJ.uc
