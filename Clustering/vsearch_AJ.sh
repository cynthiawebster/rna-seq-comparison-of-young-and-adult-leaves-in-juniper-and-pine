#!/bin/bash
#BATCH --job-name=vsearch_AJ
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch_AJ"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../Coding_Region/trinity_combine_AJ.fasta.transdecoder.cds \
        --id 0.90 \
        --centroids centroids_AJ.fasta \
        --uc clusters_AJ.uc
