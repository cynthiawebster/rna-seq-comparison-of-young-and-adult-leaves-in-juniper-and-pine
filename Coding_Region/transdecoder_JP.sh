#!/bin/bash
#SBATCH --job-name=transdecoder_JP
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_JP"


cat ../Assembly/trinity_PC1J_index12.Trinity.fasta ../Assembly/trinity_PC2J_index35.Trinity.fasta ../Assembly/trinity_PC3J_index7.Trinity.fasta >> ../Assembly/trinity_combine_JP.fasta



module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../Assembly/trinity_combine_JP.fasta

hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm trinity_combine_JP.fasta.transdecoder_dir/longest_orfs.pep


TransDecoder.Predict -t ../Assembly/trinity_combine_JP.fasta --no_refine_starts  --retain_pfam_hits pfam.domtblout 
