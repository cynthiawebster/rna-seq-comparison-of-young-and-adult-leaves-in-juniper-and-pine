#!/bin/bash
#SBATCH --job-name=transdecoder_AP
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_AP"


cat ../Assembly/trinity_PC1A_index2.Trinity.fasta ../Assembly/trinity_PC2A_index4.Trinity.fasta ../Assembly/trinity_PC3A_index6.Trinity.fasta >> ../Assembly/trinity_combine_AP.fasta



module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../Assembly/trinity_combine_AP.fasta

hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm trinity_combine_AP.fasta.transdecoder_dir/longest_orfs.pep


TransDecoder.Predict -t ../Assembly/trinity_combine_AP.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout  
