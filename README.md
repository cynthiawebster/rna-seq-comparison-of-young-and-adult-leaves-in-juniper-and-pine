## Comparing the RNA Sequence of Juvenile and Adult Leaves in Juniper and Pine

## Introduction
Juniper leaves are needle-like when they are young; however, upon reaching maturity, they undergo a major transition causing them to be awl-shaped. 
Meanwhile, pine leaves will remain needle-shaped throughout their entire life. The purpose of this project is to compare four sets of data -- 
the juvenile and adult leaves of juniper and pine in an RNA-seq by constructing a reference gene set de novo. 
<br>
The original study can be accessed here: [2015 Proposal](https://gitlab.com/cynthiawebster/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/blob/master/CompleteUCMEXUS.es.en.pdf)
<br>
### Directory Layout
The data used in this project can be accessed in the Xanadu cluster at the following directory:
`/labs/Wegrzyn/Juniper_Pine`
<br>
To view the files use the following command:
<br>
<pre style="color: silver; background: black;">-bash-4.2$ ls /labs/Wegrzyn/Juniper_Pine
Assembly  Clustering  Coding_Region  Quality_Control rnaQUAST </pre>
Sickle was already  run on the raw data, so there was no need to re-do quality control. 
The summary of the data can be seen here: [QC_Analysis](https://gitlab.com/cynthiawebster/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/blob/master/Quality_Control/QC_Analysis.pdf)
Within the `Quality_Control` sub-directory, you will find all the trimmed fastq files of the different samples:
<pre style="color: silver; background: black;">
-bash-4.2$ ls Quality_Control/
all_JPJ_JF1A_index6_R1_trimmed.fastq      all_JPJ_JF2J_index5_R2_trimmed.fastq       all_JPJ_PC1A_index2_trimmed_single.fastq   all_JPJ_PC3A_index6_R1_trimmed.fastq
all_JPJ_JF1A_index6_R2_trimmed.fastq      all_JPJ_JF2J_index5_trimmed_single.fastq   all_JPJ_PC1J_index12_R1_trimmed.fastq      all_JPJ_PC3A_index6_R2_trimmed.fastq
all_JPJ_JF1A_index6_trimmed_single.fastq  all_JPJ_JF3A_index12_R1_trimmed.fastq      all_JPJ_PC1J_index12_R2_trimmed.fastq      all_JPJ_PC3A_index6_trimmed_single.fastq
all_JPJ_JF1J_index4_R1_trimmed.fastq      all_JPJ_JF3A_index12_R2_trimmed.fastq      all_JPJ_PC1J_index12_trimmed_single.fastq  all_JPJ_PC3J_index7_R1_trimmed.fastq
all_JPJ_JF1J_index4_R2_trimmed.fastq      all_JPJ_JF3A_index12_trimmed_single.fastq  all_JPJ_PC2A_index4_R1_trimmed.fastq       all_JPJ_PC3J_index7_R2_trimmed.fastq
all_JPJ_JF1J_index4_trimmed_single.fastq  all_JPJ_JF3J_index2_R1_trimmed.fastq       all_JPJ_PC2A_index4_R2_trimmed.fastq       all_JPJ_PC3J_index7_trimmed_single.fastq
all_JPJ_JF2A_index7_R1_trimmed.fastq      all_JPJ_JF3J_index2_R2_trimmed.fastq       all_JPJ_PC2A_index4_trimmed_single.fastq   multiple_sickle.sh
all_JPJ_JF2A_index7_R2_trimmed.fastq      all_JPJ_JF3J_index2_trimmed_single.fastq   all_JPJ_PC2J_index35_R1_trimmed.fastq      multiple_sickle.sh~
all_JPJ_JF2A_index7_trimmed_single.fastq  all_JPJ_PC1A_index2_R1_trimmed.fastq       all_JPJ_PC2J_index35_R2_trimmed.fastq      PC.R1.trimmed.fastq
all_JPJ_JF2J_index5_R1_trimmed.fastq      all_JPJ_PC1A_index2_R2_trimmed.fastq       all_JPJ_PC2J_index35_trimmed_single.fastq  PC.R2.trimmed.fastq</pre>

These are the associated names of all the samples given in the directory:
<br>


| Sample | Organism | Leaf Age | Species |  
| :------: | :------: |  :------: |  :------: |
| JPJ_JF1A | Juniper |Adult |*Juniperus faccida* |
| JPJ_JF2A | Juniper |Adult |*Juniperus faccida* | 
| JPJ_JF3A | Juniper |Adult |*Juniperus faccida* |
| JPJ_JF1J | Juniper |Juvenile |*Juniperus faccida* |
| JPJ_JF2J | Juniper |Juvenile |*Juniperus faccida* |
| JPJ_JF3J | Juniper |Juvenile |*Juniperus faccida* |
| JPJ_PC1A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC2A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC3A | Pine    |Adult    | *Pinus cembroides*  |
| JPJ_PC1J | Pine    |Juvenile | *Pinus cembroides*  |
| JPJ_PC2J | Pine    |Juvenile | *Pinus cembroides*  |
| JPJ_PC3J | Pine    |Juvenile | *Pinus cembroides*  |

<br>

## 1. Assembling the Transcriptome with Trinity
Because the genomes are not given, we had to construct them de novo using a transcript assembly software called Trinity. 
Trinity uses the trimmed fastq files from the Quality_Control directory to construct the transcriptome. 
Several scripts were used to run this software due to the immense RAM and time required; four libraries were run per job. In the `Assembly` sub-directory
you will find three shell scripts: `trinity.sh`, `trinity2.sh` and `trinity3.sh`. Here is a preview of what `trinity.sh` looks like:
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=128G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "trinity.sh"

module load trinity/2.6.6

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1A_index6_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1A_index6_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1A_index6 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF1J_index4_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF1J_index4_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF1J_index4 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2A_index7_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2A_index7_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2A_index7 \
        --full_cleanup

Trinity --seqType fq \
        --left ../Quality_Control/all_JPJ_JF2J_index5_R1_trimmed.fastq \
        --right ../Quality_Control/all_JPJ_JF2J_index5_R2_trimmed.fastq \
        --min_contig_length 300 \
        --CPU 36 \
        --max_memory 100G \
        --output trinity_JF2J_index5 \
        --full_cleanup </pre>
        
Following the Trinity runs, the resulting file structure in the `Assembly` subdirectory was as follows:
<pre style="color: silver; background: black;">

-bash-4.2$ ls 
trinity_JF1A_index6.Trinity.fasta
trinity_JF1A_index6.Trinity.fasta.gene_trans_map
trinity_JF1J_index4.Trinity.fasta
trinity_JF1J_index4.Trinity.fasta.gene_trans_map
trinity_JF2A_index7.Trinity.fasta
trinity_JF2A_index7.Trinity.fasta.gene_trans_map
trinity_JF2J_index5.Trinity.fasta
trinity_JF2J_index5.Trinity.fasta.gene_trans_map
trinity_JF3A_index12.Trinity.fasta
trinity_JF3A_index12.Trinity.fasta.gene_trans_map
trinity_JF3J_index2.Trinity.fasta
trinity_JF3J_index2.Trinity.fasta.gene_trans_map
trinity_PC1A_index2.Trinity.fasta
trinity_PC1A_index2.Trinity.fasta.gene_trans_map
trinity_PC1J_index12.Trinity.fasta
trinity_PC1J_index12.Trinity.fasta.gene_trans_map
trinity_PC2A_index4.Trinity.fasta
trinity_PC2A_index4.Trinity.fasta.gene_trans_map
trinity_PC2J_index35.Trinity.fasta
trinity_PC2J_index35.Trinity.fasta.gene_trans_map
trinity_PC3A_index6.Trinity.fasta
trinity_PC3A_index6.Trinity.fasta.gene_trans_map
trinity_PC3J_index7.Trinity.fasta
trinity_PC3J_index7.Trinity.fasta.gene_trans_map 
trinity.sh
trinity2.sh
trinity3.sh </pre>

### Run rnaQUAST
To summarize the assembled transcripts, rnaQUAST was run on each Trinity output.
The script, found in the `rnaQUAST` sub-directory looks like this: 

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=rnaQuast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "rnaQUAST"


module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1


rnaQUAST.py --transcripts ../Assembly/trinity_JF1A_index6.Trinity.fasta ../Assembly/trinity_JF2A_index7.Trinity.fasta ../Assembly/trinity_JF3A_index12.Trinity.fasta ../Assembly/trinity_PC1A_index2.Trinity.fasta ../Assembly/trinity_PC2A_index4.Trinity.fasta ../Assembly/trinity_PC3A_index6.Trinity.fasta ../Assembly/trinity_JF1J_index4.Trinity.fasta ../Assembly/trinity_JF2J_index5.Trinity.fasta ../Assembly/trinity_JF3J_index2.Trinity.fasta ../Assembly/trinity_PC1J_index12.Trinity.fasta ../Assembly/trinity_PC2J_index35.Trinity.fasta ../Assembly/trinity_PC3J_index7.Trinity.fasta  \
        --gene_mark \
        --threads 8 \
        --output_dir results </pre>
The results can be viewed here: [Trinty Transcript Summary](https://gitlab.com/cynthiawebster/rna-seq-comparison-of-young-and-adult-leaves-in-juniper-and-pine/blob/master/rnaQUAST/Trinity_short_report.pdf)

## 2. Identifying Coding Region with TransDecoder
TransDecoder is a software that identifies coding regions in the transcript sequences
generated by Trinity. Within the `Coding_Region` sub-directory you will find the scripts to run our job. 
The script was split into four parts for efficieny -- AJ, JJ, AP and JP.
Let's take a look at `transcoder_JJ.sh`
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=transdecoder_JJ
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=150G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=cynthia.webster@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "transdecoder_JJ"


cat ../Assembly/trinity_JF1J_index6.Trinity.fasta ../Assembly/trinity_JF2J_index7.Trinity.fasta ../Assembly/trinity_JF3J_index12.Trinity.fasta >> ../Assembly/trinity_combine_JJ.fasta



module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t ../Assembly/trinity_combine_JJ.fasta

hmmscan --cpu 16 --domtblout pfam.domtblout /isg/shared/databases/Pfam/Pfam-A.hmm trinity_combine_JJ.fasta.transdecoder_dir/longest_orfs.pep


TransDecoder.Predict -t ../Assembly/trinity_combine_JJ.fasta --no_refine_starts --retain_pfam_hits pfam.domtblout
 </pre>

TransDecoder works by first extracting the long open reading frames with `TransDecoder.LonOrfs`.
It then identifies ORFs with homology to known proteins with pfam searches, illustrated by `hmmscan`. Finally `TransDecoder.Predict`  predicts the likely coding regions 
in the sequences. 
<Br>
The job would not complete unless "--no_refine_starts" was included in the final line of the script. 
Overall, the following files were generated from this script:
<pre style="color: silver; background: black;">
trinity_combine_JJ.fasta.transdecoder_dir
trinity_combine_JJ.fasta.transdecoder_dir.__checkpoints
trinity_combine_JJ.fasta.transdecoder_dir.__checkpoints_longorfs
trinity_combine_JJ.fasta.transdecoder.bed
trinity_combine_JJ.fasta.transdecoder.cds
trinity_combine_JJ.fasta.transdecoder.gff3
trinity_combine_JJ.fasta.transdecoder.pep </pre>


Afterwards, rnaQUAST was run on the TransDecoder results for the separate age groups for juniper and pine (4) as well as for 
combined age groups (2) -- found in the rnaQUAST directory.

## 3. Clustering with VSEARCH
The next step was to remove redundant transcripts and shorten the transcriptome so that it is easier to work with. In the sub-directory `Clustering` you will find the scripts `vsearch_AJ.sh`,
`vsearch_JJ.sh`, `vsearch_AP.sh` and `vsearch_JP.sh`. Here is `vsearch_AJ.sh`:
<pre style="color: silver; background: black;">

#!/bin/bash
#SBATCH --job-name=vsearch_AJ
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "vsearch_AJ"

module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
        --cluster_fast ../Coding_Regions/trinity_combine_AJ.fasta.transdecoder.cds \
        --id 0.90 \
        --centroids centroids_AJ.fasta \
        --uc clusters_AJ.uc</pre>
        
        



